package http;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import util.Util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mad_ass2_3383484.R;

import android.util.Log;

public class DistanceMatrixCode {

    public static final int TIME_OUT = 3000; // 3 seconds

    public static ValueHolder requestDurationData(double initLong, double initLat, double destLong, double destLat) {
        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, TIME_OUT);
        HttpClient httpclient = new DefaultHttpClient(httpParams);
        String url = constructURL(initLat, initLong, destLat, destLong);
        HttpGet getRequest = new HttpGet(url);
        ValueHolder newDistanceValues = null;

        String responseBody = "";

        Log.i(Util.LOG_TAG, url);

        try {
            responseBody = httpclient.execute(getRequest, new BasicResponseHandler());
            newDistanceValues = parseJsonToValues(responseBody);
        } catch (HttpQueryLimitException e) {
            newDistanceValues = new ValueHolder("(somehow) exceeded 2.5k daily requests", -1, ValueHolder.Status.FOUND);
        } catch (Exception e) {
            newDistanceValues = new ValueHolder("Invalid Long/Lat coords", -1, ValueHolder.Status.NOT_FOUND);
            Log.e(Util.LOG_TAG, responseBody);
            e.printStackTrace();
        }
        return newDistanceValues;
    }

    private static ValueHolder parseJsonToValues(String responseBody) throws Exception {
        JsonElement jelement = new JsonParser().parse(responseBody);
        JsonObject jobject = jelement.getAsJsonObject();

        if (jobject.get("status").toString().equals("\"OVER_QUERY_LIMIT\"")) {
            throw new HttpQueryLimitException();
        }

        JsonArray rows = jobject.getAsJsonArray("rows");
        JsonObject rowObject = rows.get(0).getAsJsonObject();

        JsonArray element = rowObject.getAsJsonArray("elements");
        JsonObject elementObject = element.get(0).getAsJsonObject();

        JsonObject duration = elementObject.getAsJsonObject("duration");

        String result = duration.get("text").toString();
        Long value = duration.get("value").getAsLong();

        return new ValueHolder(result, value, ValueHolder.Status.FOUND);
    }

    private static String constructURL(double initLong, double initLat, double destLong, double destLat) {
        return "https://maps.googleapis.com/maps/api/distancematrix/json?" + "origins=" + initLong + "," + initLat + "&destinations=" + destLong + "," + destLat + "&units=metric"
                + "&key=" + R.string.DistanceMatrixKey;
    }

}
