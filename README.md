# Event Manager Android Application 
Written for RMIT University module Mobile Application Development 2014.  

An android application which manages user's events. Users can add events and their details into the app, which includes [Title], [Time], [Location], [Note], etc. The app will estimate the travel time between the current location and the event's location using Google Distance Matrix API. Using the estimated travel time, the app will alert the user when it is the best time to leave for the next event.
### Functionality and features
* Users can add multiple events, as well as edit and delete existing events.
* The app will alert users when it's the perfect time to travel to the next event, using GPS and Google Distance Matrix API.
* When adding new events, the app will check if the new event can fit into the current schedule (also takes into account travel times).  
* Modular design with Hierarchical MVC architecture.
* Has multiple views (calendar view and list view) on different fragments.
* Uses persistent storage with SQLite.
* Only queries the Google API when there's Internet connectivity and uses GPS only if its available.