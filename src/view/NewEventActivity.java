package view;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import view.controller.EventOnDateSetListener;
import view.controller.EventOnTimeSetListener;
import view.controller.TextViewOnDateClickListener;
import view.controller.TextViewOnTimeClickListener;
import model.EventInformation;
import model.BaseEventModel;
import model.EventInformationBuilder;

import model.ResourceLocator;

import com.mad_ass2_3383484.R;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewEventActivity extends Activity {

    private Button btnConfirm;

    private Button btnDate;
    private Button btnTime;
    private EditText txtTitle;
    private EditText txtLong;
    private EditText txtLat;
    private EditText txtNote;
    private EditText txtVenue;

    private Calendar cal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_event);

        setTitle(R.string.action_new_event);
        cal = Calendar.getInstance();

        String dayString = getIntent().getStringExtra(BaseEventModel.EVENT_ID);
        if (dayString != null) {
            int day = Integer.parseInt(dayString);
            cal.set(Calendar.DATE, day);
        }

        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnDate = (Button) findViewById(R.id.btnNewEventDate);
        btnTime = (Button) findViewById(R.id.btnNewEventTime);
        txtTitle = (EditText) findViewById(R.id.txtNewEventTitle);
        txtLong = (EditText) findViewById(R.id.txtNewEventLong);
        txtLat = (EditText) findViewById(R.id.txtNewEventLat);
        txtNote = (EditText) findViewById(R.id.txtNewEventNote);
        txtVenue = (EditText) findViewById(R.id.txtNewEventVenue);

        final SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.UK);

        btnDate.setText(dateFormat.format(cal.getTime()));

        EventOnDateSetListener date = new EventOnDateSetListener(cal, btnDate, dateFormat);
        TextViewOnDateClickListener dateClickListener = new TextViewOnDateClickListener(cal, date, this);

        btnDate.setOnClickListener(dateClickListener);

        final SimpleDateFormat timeFormat = new SimpleDateFormat("h:m a", Locale.UK);
        btnTime.setText(timeFormat.format(cal.getTime()));

        TimePickerDialog.OnTimeSetListener time = new EventOnTimeSetListener(cal, btnTime, timeFormat);
        TextViewOnTimeClickListener timeClickListener = new TextViewOnTimeClickListener(this, time, cal);
        btnTime.setOnClickListener(timeClickListener);

        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmCreation();
                finish();
            }
        });
    }

    private void confirmCreation() {
        String title;
        String longitude, lat;
        String note;
        String venue;
        Calendar time;

        title = txtTitle.getText().toString();
        longitude = txtLong.getText().toString();
        lat = txtLat.getText().toString();
        note = txtNote.getText().toString();
        venue = txtVenue.getText().toString();
        time = cal;

        EventInformation e = new EventInformationBuilder().setTitle(title).setNote(note).setVenue(venue).setTime(time).setLong(longitude).setLat(lat).buildEvent();

        ResourceLocator.getEventModel().addEvent(e, this.getBaseContext());
        Toast.makeText(this, R.string.toastConfirmNewEvent, Toast.LENGTH_LONG).show();
        setResult(Activity.RESULT_OK);
    }

}
