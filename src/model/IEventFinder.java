package model;

public interface IEventFinder {

    public EventInformation getEvent(int pos);

    public int eventToPosition(EventInformation e);

}
