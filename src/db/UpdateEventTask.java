package db;

import util.Util;
import model.EventInformation;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateEventTask extends AsyncTask<EventInformation, Integer, Long> {

    Context context;

    public UpdateEventTask(Context context) {
        this.context = context;
    }

    @Override
    protected Long doInBackground(EventInformation... event) {
        EventDatabaseHelper dbHelper = new EventDatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        String selection = EventDatabaseContract.EventTable._ID + " LIKE ?";

        long id = 0;

        db.beginTransaction();
        try {
            long ids = event[0].getID();
            String[] selectionArgs = { String.valueOf(ids) };
            values = event[1].populateContentValues(values);
            int numRows = db.update(EventDatabaseContract.EventTable.TABLE_NAME, values, selection, selectionArgs);

            if (numRows != 1) {
                Log.e(Util.LOG_TAG, "Error when updating. Number of rows affected: " + numRows + ", first id:|" + ids + "| second id:" + event[1].getID());
            }

            db.setTransactionSuccessful();
        } catch (IllegalStateException e) {
            Log.i(Util.LOG_TAG, "UpdateEventTask transaction cannot be completed " + e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

        return id;
    }

}
