package util;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.AsyncTask;
import android.os.Build;

public class Util {

    public static final String LOG_TAG = "Something";

    private static final Type LIST_STRING_TYPE = new TypeToken<List<String>>() {
    }.getType();

    public static List<String> parseJsonToList(String json) {
        return new Gson().fromJson(json, LIST_STRING_TYPE);
    }

    public static String parseListToJson(List<String> list) {
        return new Gson().toJson(list, LIST_STRING_TYPE);
    }

    public static <P, T extends AsyncTask<P, ?, ?>> void execute(T task, P... params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        } else {
            task.execute(params);
        }
    }

    public static boolean isSameDay(Calendar c1, Calendar c2) {
        return c1 != null && c2 != null && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) && c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
    }
}
