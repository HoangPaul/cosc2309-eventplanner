package util;

/* Any class implementing this needs to keep track of its own observers */
public interface Observable<T> {

    public void updateObservers(T o);

    public T addObservers(Observer<T> o);
}
