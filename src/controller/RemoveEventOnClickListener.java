package controller;

import model.EventInformation;
import model.EventModelException;
import model.ResourceLocator;
import android.view.View;
import android.view.View.OnClickListener;

public class RemoveEventOnClickListener implements OnClickListener {

    private EventInformation e;

    public RemoveEventOnClickListener(EventInformation e) {
        this.e = e;
    }

    @Override
    public void onClick(View v) {
        try {
            ResourceLocator.getEventModel().removeEvent(e, v.getContext());
        } catch (EventModelException e) {
            e.printStackTrace();
        }
    }

}
