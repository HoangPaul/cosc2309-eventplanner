package http;

import java.util.Calendar;

import util.Util;
import model.ResourceLocator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

/*
 * Code derived from:
 * http://stackoverflow.com/questions/15698790/broadcast-receiver-for-checking-internet-connection-in-android-app?rq=1
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifi != null && wifi.isAvailable()) || (mobile != null && mobile.isAvailable())) {
            Log.i(Util.LOG_TAG, "Internet available");
            if (Calendar.getInstance().getTimeInMillis() - ResourceLocator.getEventModel().getTimeSinceLastUpdate().getTimeInMillis() > 5 * 60 * 1000) {
                /*
                 * it's been greater than the threshold, we'll update straight
                 * away
                 */
                ResourceLocator.getEventModel().updateAllEventDistance(context);
            }
        } else {
            Log.i(Util.LOG_TAG, "Internet unavailable");
        }
    }

}
