package view.model;

import java.util.ArrayList;
import java.util.Calendar;
import util.Observer;
import util.Util;

import com.mad_ass2_3383484.R;

import controller.RemoveEventOnClickListener;
import model.EventInformation;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAllEventArrayAdapter<T extends ViewableModel<T>> extends ListEventArrayAdapter implements Observer<T> {

    enum ViewState {
        Separator, Event
    };

    private static class ViewHolder {
        public ViewState state;
        public int eventPosition;
    }

    Context context;
    ViewHolder viewStateHolders[];
    ViewableModel<T> model;

    public ListAllEventArrayAdapter(Context context, int resource, ViewableModel<T> model) {
        super(context, resource, new ArrayList<EventInformation>());
        this.model = model.addObservers(this);
        this.events = model.getEvents();
        populateStateList();
    }

    @Override
    public int getCount() {
        if (this.events != null) {
            return this.events.size() + numUniqueDays();
        } else {
            return 0;
        }
    }

    @Override
    public EventInformation getEvent(int pos) {
        EventInformation event;
        if (viewStateHolders[pos].state == ViewState.Separator) {
            event = null;
        } else {
            event = events.get(viewStateHolders[pos].eventPosition);
        }
        return event;
    }

    private int numUniqueDays() {
        int counter = 0;
        Calendar c = null;
        for (int i = 0; i < this.events.size(); i++) {
            if (!Util.isSameDay(c, this.events.get(i).getCalendar())) {
                counter++;
            }
            c = this.events.get(i).getCalendar();
        }
        return counter;
    }

    private void populateStateList() {
        viewStateHolders = new ViewHolder[this.events.size() + numUniqueDays()];
        int separatorCounter = 0;
        Calendar prevCal = null;

        for (int i = 0; i < viewStateHolders.length; i++) {
            boolean isSeparatorPostion = !Util.isSameDay(prevCal, this.events.get(i - separatorCounter).getCalendar());
            prevCal = this.events.get(i - separatorCounter).getCalendar();

            viewStateHolders[i] = new ViewHolder();
            viewStateHolders[i].eventPosition = i - separatorCounter;

            if (isSeparatorPostion) {
                viewStateHolders[i].state = ViewState.Separator;
                separatorCounter++;
            } else {
                viewStateHolders[i].state = ViewState.Event;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return viewStateHolders[position].state.ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return ViewState.values().length;
    }

    private View populateEventView(int position, View convertView, ViewGroup parent) {
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtListEventRowTitle);
        TextView txtTime = (TextView) convertView.findViewById(R.id.txtListEventRowTime);
        TextView txtLocation = (TextView) convertView.findViewById(R.id.txtListEventRowLocation);
        TextView txtTiming = (TextView) convertView.findViewById(R.id.txtListEventRowTiming);
        ImageButton btnDelete = (ImageButton) convertView.findViewById(R.id.btnListEventRow);
        ImageView imgNote = (ImageView) convertView.findViewById(R.id.imgListEventNote);
        ImageView imgLocation = (ImageView) convertView.findViewById(R.id.imgListEventLocation);

        final EventInformation e = events.get(viewStateHolders[position].eventPosition);
        txtTitle.setText(e.getTitle());
        txtTime.setText(e.getTimeString());
        switch (e.getValueHolder().status) {
        case EMPTY:
            hideText(txtLocation);
            hideText(txtTiming);
            imgLocation.setVisibility(View.INVISIBLE);
            break;
        case NOT_FOUND:
            showText(txtLocation, R.string.txtLocationNotFound);
            hideText(txtTiming);
            imgLocation.setVisibility(View.VISIBLE);
            break;
        case FOUND:
            showText(txtLocation, "From " + e.getPreviousLocation() + " to " + e.getTitle() + ": " + e.getDurationText() + " via car");
            imgLocation.setVisibility(View.VISIBLE);
            if (!e.isAbleToMakeNextEvent()) {
                showText(txtTiming, R.string.txtUnableToArriveOnTime);
                txtTiming.setTextColor(Color.RED);
            } else {
                showText(txtTiming, R.string.txtAbleToArriveOnTime);
                txtTiming.setTextColor(Color.GREEN);
            }
            break;
        }
        btnDelete.setOnClickListener(new RemoveEventOnClickListener(e));

        if (e.hasNote()) {
            imgNote.setVisibility(View.VISIBLE);
        } else {

        }

        return convertView;

    }

    private void showText(TextView t, int i) {
        t.setText(i);
        t.setVisibility(View.VISIBLE);
    }

    private void showText(TextView t, String msg) {
        t.setText(msg);
        t.setVisibility(View.VISIBLE);
    }

    private void hideText(TextView t) {
        t.setText("");
        t.setVisibility(View.INVISIBLE);
    }

    private View populateSeparatorView(int position, View convertView, ViewGroup parent) {
        TextView txtSeparator = (TextView) convertView.findViewById(R.id.txtListSeparatorDate);
        txtSeparator.setText(events.get(viewStateHolders[position].eventPosition).getLongDateString());
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == ViewState.Separator.ordinal()) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_separator, parent, false);
            }
            populateSeparatorView(position, convertView, parent);
        } else {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_event_row, parent, false);
            }
            populateEventView(position, convertView, parent);
        }
        return convertView;
    }

    @Override
    public void update(T o) {
        this.events = o.getEvents();
        populateStateList();
        notifyDataSetChanged();
    }

}
