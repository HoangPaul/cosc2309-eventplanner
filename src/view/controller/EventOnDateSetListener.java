package view.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.TextView;

public class EventOnDateSetListener implements DatePickerDialog.OnDateSetListener {

    Calendar cal;
    TextView txtView;
    SimpleDateFormat sdf;

    public EventOnDateSetListener(Calendar cal, TextView txtView, SimpleDateFormat sdf) {
        this.cal = cal;
        this.txtView = txtView;
        this.sdf = sdf;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        txtView.setText(sdf.format(cal.getTime()));
    }
}
