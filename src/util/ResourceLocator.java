package model;

import view.model.CalendarModel;
import view.model.ListModel;

public class ResourceLocator {

    private static CalendarModel calendarModel;
    private static AbstractEventModel eventModel;
    private static ListModel listModel;

    public static CalendarModel getCalendarModel() {
        if (calendarModel == null) {
            calendarModel = new CalendarModel(getEventModel());
        }
        return calendarModel;
    }

    public static AbstractEventModel getEventModel() {
        if (eventModel == null) {
            // eventModel = new BaseEventModel();
            eventModel = new DatabaseEventModel();
        }
        return eventModel;
    }

    public static ListModel getListModel() {
        if (listModel == null) {
            listModel = new ListModel(getEventModel());
        }
        return listModel;
    }

    /* setters to inject new models for testing */
    public static void setCalendarModel(CalendarModel newModel) {
        calendarModel = newModel;
    }

    public static void setEventModel(AbstractEventModel newModel) {
        eventModel = newModel;
    }
}
