package view.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import model.EventInformation;
import util.Observable;
import util.Observer;
import util.Util;

public class CalendarModel extends ViewableModel<CalendarModel> {

    private Calendar calendar;
    private Calendar dayCalendar;
    private List<EventInformation> dayEvents;

    public CalendarModel(Observable<List<EventInformation>> observable) {
        super(observable);
        calendar = Calendar.getInstance();
        dayCalendar = Calendar.getInstance();
        dayEvents = new ArrayList<EventInformation>();
    }

    public List<EventInformation> getAllEvents() {
        return events;
    }

    @Override
    public List<EventInformation> getEvents() {
        return dayEvents;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
        this.dayEvents = getEventsOnDay(calendar);
        this.updateObservers(this);
    }

    public Calendar getCalendar() {
        return this.calendar;
    }

    public Calendar getDayCalendar() {
        return this.dayCalendar;
    }

    public String getMonthName() {
        return this.calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
    }

    public void setToPreviousMonth() {
        this.calendar.add(Calendar.MONTH, -1);
        this.dayEvents = getEventsOnDay(calendar);
        this.dayCalendar = null;
        this.updateObservers(this);
    }

    public void setToNextMonth() {
        this.calendar.add(Calendar.MONTH, 1);
        this.dayEvents = getEventsOnDay(calendar);
        this.dayCalendar = null;
        this.updateObservers(this);
    }

    public List<EventInformation> getEventsOnDay(Calendar o) {
        ArrayList<EventInformation> dayEvents = new ArrayList<EventInformation>();
        for (int i = 0; i < events.size(); i++) {
            if (Util.isSameDay(o, events.get(i).getCalendar())) {
                dayEvents.add(events.get(i));
            }
        }
        if (dayCalendar == null)
            dayCalendar = Calendar.getInstance();
        dayCalendar.set(Calendar.DATE, o.get(Calendar.DATE));
        return dayEvents;
    }

    @Override
    public CalendarModel addObservers(Observer<CalendarModel> o) {
        obs.add(o);
        return this;
    }

    @Override
    public void update(List<EventInformation> o) {
        this.events = o;
        this.dayEvents = getEventsOnDay(calendar);
        updateObservers(this);
    }

}
