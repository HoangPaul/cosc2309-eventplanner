package model;

import http.DistanceMatrixHelper;
import http.ThreadCallback;
import http.ValueHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import util.Util;
import db.EventDatabaseContract;
import android.content.ContentValues;

public class EventInformation {

    public static final double DEFAULT_LONG_VALUE = 0;
    public static final double DEFAULT_LAT_VALUE = 0;

    private long id;
    private final String title;
    private final Calendar time;
    private final String venue;
    private final String note;
    private final ArrayList<String> attendees;

    private final double destLong;
    private final double destLat;

    private double durationValue;
    private String durationText;

    private DistanceMatrixHelper dmh;

    private String previousLocation;
    private ValueHolder valueHolder;

    private boolean isAbleToMakeNextEvent;

    public EventInformation(long id, String title, Calendar time, String venue, String note, double destLong, double destlat, List<String> attendees) {
        this.id = id;
        this.title = title == null ? "" : title;
        this.time = time == null ? Calendar.getInstance() : (Calendar) time.clone();
        this.venue = venue == null ? "" : venue;
        this.note = note == null ? "" : note;
        this.destLong = destLong;
        this.destLat = destlat;
        this.attendees = attendees == null ? new ArrayList<String>() : new ArrayList<String>(attendees);
        this.durationText = "";
        this.durationValue = 0;
        this.previousLocation = "Current Location";
        this.valueHolder = ValueHolder.EMPTY_HOLDER;
        this.isAbleToMakeNextEvent = true;
        dmh = new DistanceMatrixHelper();
    }

    public EventInformation(String id, String title, String time, String venue, String note, String destLong, String destlat, String attendees) {
        this(Long.parseLong(id), title, null, venue, note, Double.parseDouble(destLong), Double.parseDouble(destlat), Util.parseJsonToList(attendees));

        this.time.setTimeInMillis(Long.parseLong(time));
    }

    public EventInformation(EventInformation toBeCloned) {
        this(toBeCloned.getID(), toBeCloned.getTitle(), toBeCloned.getCalendar(), toBeCloned.getVenue(), toBeCloned.getNote(), toBeCloned.getLong(), toBeCloned.getLat(),
                toBeCloned.getAttendees());
    }

    public long getID() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getVenue() {
        return this.venue;
    }

    public Calendar getCalendar() {
        return this.time;
    }

    public String getNote() {
        return this.note;
    }

    public double getLong() {
        return this.destLong;
    }

    public double getLat() {
        return this.destLat;
    }

    public double getDurationValue() {
        return this.durationValue;
    }

    public String getDurationText() {
        return this.durationText;
    }

    public String getPreviousLocation() {
        return this.previousLocation;
    }

    public ValueHolder getValueHolder() {
        return this.valueHolder;
    }

    public List<String> getAttendees() {
        return this.attendees;
    }

    public boolean isAbleToMakeNextEvent() {
        return this.isAbleToMakeNextEvent;
    }

    public boolean hasVenue() {
        return this.venue != null && !this.venue.isEmpty();
    }

    public boolean hasNote() {
        return this.note != null && !this.note.isEmpty();
    }

    public boolean hasLocation() {
        return this.destLong != DEFAULT_LONG_VALUE && this.destLat != DEFAULT_LAT_VALUE;
    }

    public boolean hasDuration() {
        return this.durationText != null && !this.durationText.isEmpty();
    }

    public String getLongDateString() {
        SimpleDateFormat ss = new SimpleDateFormat("d MMMM yyyy", Locale.UK);
        return ss.format(time.getTime());
    }

    public String getShortDateString() {
        SimpleDateFormat ss = new SimpleDateFormat("d MMM yyyy", Locale.UK);
        return ss.format(time.getTime());
    }

    public String getTimeString() {
        SimpleDateFormat ss = new SimpleDateFormat("hh:mm aaa", Locale.UK);
        return ss.format(time.getTime());
    }

    public ContentValues populateContentValues(ContentValues values) {
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_TIME, String.valueOf(this.time.getTimeInMillis()));
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_TITLE, this.title);
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_LONG, String.valueOf(this.destLong));
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_LAT, String.valueOf(this.destLat));
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_VENUE, this.venue);
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_NOTE, this.note);
        values.put(EventDatabaseContract.EventTable.COLUMN_NAME_ATTENDEES, Util.parseListToJson(this.attendees));
        return values;
    }

    public void updateAsyncDistanceInformation(double initLong, double initlat, final ThreadCallback<ValueHolder> callback) {
        // 37.8140000,144.9633200&destinations=-33.8678500,151.2073200
        // dmh.executeHttpRequest(-37.8140000, 144.9633200, -33.8678500,
        // 151.2073200);
        dmh.setCallBack(new ThreadCallback<ValueHolder>() {
            @Override
            public void functionCallback(ValueHolder... params) {
                updateValues(params[0]);
                callback.functionCallback(params[0]);
            }
        });
        dmh.executeHttpAsyncTaskRequest(initLong, initlat, destLong, destLat);
    }

    public boolean isTimeToLeave(Calendar currTime) {
        Calendar c = (Calendar) currTime.clone();
        c.add(Calendar.SECOND, (int) this.durationValue);
        return c.compareTo(this.time) >= 0;
    }

    public void setIsAbleToMakeNextEvent(boolean b) {
        isAbleToMakeNextEvent = b;
    }

    private void updateValues(ValueHolder values) {
        if (values != null) {
            this.durationValue = values.value;
            this.durationText = values.text.replace("\"", "");
            this.valueHolder = values;
        }
    }

    public void setPreviousLocation(String loc) {
        previousLocation = loc;
    }

    public void setID(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object e) {
        EventInformation event = (EventInformation) e;
        return this.toString().equals(event.toString());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append(title).append(",").append(time.toString()).append(",").append(venue).toString();
    }

    @Override
    public int hashCode() {
        return toString().hashCode(); // bleh im lazy
    }

}
