package http;

/*  Make this immutable. Helps with concurrency */
public final class ValueHolder {

    public static enum Status {
        FOUND, NOT_FOUND, EMPTY
    }

    public static final ValueHolder EMPTY_HOLDER = new ValueHolder("", 0, Status.EMPTY);
    public static final ValueHolder SAME_LOCATION_RESULT = new ValueHolder("0 hours 0 mins", 0, Status.FOUND);

    public final String text;
    public final double value;
    public final Status status;

    public ValueHolder(String t, double v, Status s) {
        this.text = t;
        this.value = v;
        this.status = s;
    }
}
