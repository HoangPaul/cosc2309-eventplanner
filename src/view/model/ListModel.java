package view.model;

import java.util.List;

import model.EventInformation;
import util.Observable;
import util.Observer;

public class ListModel extends ViewableModel<ListModel> {

    public ListModel(Observable<List<EventInformation>> observable) {
        super(observable);
    }

    @Override
    public void update(List<EventInformation> o) {
        this.events = o;
        updateObservers(this);
    }

    @Override
    public ListModel addObservers(Observer<ListModel> o) {
        obs.add(o);
        return this;
    }
}
