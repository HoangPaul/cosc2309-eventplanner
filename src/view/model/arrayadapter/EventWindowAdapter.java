package view.model;

import java.util.Map;

import model.EventInformation;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.mad_ass2_3383484.R;

/**
 * Code derived from Matt Witherow's example See MapActivity.java for
 * description
 */
public class EventWindowAdapter implements InfoWindowAdapter {

    Context context;
    EventInformation event;
    Map<Marker, EventInformation> mMap;

    public EventWindowAdapter(Context context, Map<Marker, EventInformation> mMap) {
        this.context = context;
        this.mMap = mMap;
    }

    @Override
    public View getInfoContents(Marker arg0) {
        return generateView(arg0);
    }

    @Override
    public View getInfoWindow(Marker arg0) {
        return generateView(arg0);
    }

    private View generateView(Marker marker) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_marker, null);

        EventInformation event = this.mMap.get(marker);

        TextView title = (TextView) view.findViewById(R.id.txtMarkerTitle);
        TextView desc = (TextView) view.findViewById(R.id.txtMarkerDescription);

        title.setText(event.getTitle());
        desc.setText(event.getShortDateString());

        return view;
    }

}
