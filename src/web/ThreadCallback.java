package http;

/* After completing some task on a thread, we can specify a callback function with this */
public interface ThreadCallback<T> {

    public void functionCallback(T... params);

}
