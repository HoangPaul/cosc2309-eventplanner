package view;

import model.ResourceLocator;
import service.HttpGetService;
import test.Test;
import util.Util;

import com.mad_ass2_3383484.R;

import controller.TabListener;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    private static final String SavedBundleString = "MainActivityIndex";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setContentView(R.layout.activity_main);
        }

        ResourceLocator.getEventModel().updateModelFromDatabase(this);

        ActionBar actionBar = this.getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        Tab tab = actionBar.newTab().setText(R.string.calendarTab).setTabListener(new TabListener<CalendarFragment>(this, "Calendar", CalendarFragment.class));
        actionBar.addTab(tab, true);

        tab = actionBar.newTab().setText(R.string.listTab).setTabListener(new TabListener<ListEventFragment>(this, "List", ListEventFragment.class));
        actionBar.addTab(tab);

        if (savedInstanceState != null) {
            int index = savedInstanceState.getInt(SavedBundleString);
            getActionBar().setSelectedNavigationItem(index);
        }

        Intent intent = new Intent(this, HttpGetService.class);
        Log.i(Util.LOG_TAG, "starting alarm manager");
        PendingIntent pintent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, HttpGetService.START_TIME, HttpGetService.REPEAT_TIME, pintent);
        Log.i(Util.LOG_TAG, "alarm manager started");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_event) {
            Intent myIntent = new Intent(this, NewEventActivity.class);
            this.startActivityForResult(myIntent, 0);
            return true;
        } else if (id == R.id.action_show_map) {
            Intent myIntent = new Intent(this, MapActivity.class);
            this.startActivityForResult(myIntent, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int i = getActionBar().getSelectedNavigationIndex();
        outState.putInt(SavedBundleString, i);
    }

    @Override
    public void onPause() {
        super.onPause();
        // stopService(new Intent(this, TimerService.class));
        /*
         * Intent intent = new Intent(this, HttpGetService.class); PendingIntent
         * pendingIntent = PendingIntent.getService(this, 0, intent,
         * PendingIntent.FLAG_UPDATE_CURRENT); AlarmManager alarmManager =
         * (AlarmManager) getSystemService(ALARM_SERVICE);
         * alarmManager.cancel(pendingIntent);
         */
    }

    @Override
    public void onResume() {
        super.onResume();
        // startService(new Intent(this, TimerService.class));
    }
}
