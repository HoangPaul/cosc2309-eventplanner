package model.controller;

import java.util.Calendar;

import model.BaseEventModel;
import util.Util;
import view.NewEventActivity;
import view.model.CalendarModel;
import view.model.GridEventArrayAdapter;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class GridOnItemClickListener implements OnItemClickListener {

    CalendarModel cm;
    GridEventArrayAdapter arrayAdapter;

    public GridOnItemClickListener(GridEventArrayAdapter arrayAdapter, CalendarModel cm) {
        this.arrayAdapter = arrayAdapter;
        this.cm = cm;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Calendar c = arrayAdapter.getDateFromPosition(position);
        if (c != null) {
            if (Util.isSameDay(cm.getDayCalendar(), c)) {
                cm.setCalendar(c);
                Intent myIntent = new Intent(view.getContext(), NewEventActivity.class);
                myIntent.putExtra(BaseEventModel.EVENT_ID, c.get(Calendar.DATE) + "");
                view.getContext().startActivity(myIntent);
            } else {
                cm.setCalendar(c);
            }
        }
        arrayAdapter.setPositionClicked(position);
    }
}
