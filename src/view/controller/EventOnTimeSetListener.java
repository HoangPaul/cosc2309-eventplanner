package view.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.TimePickerDialog;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventOnTimeSetListener implements TimePickerDialog.OnTimeSetListener {

    Calendar cal;
    TextView textView;
    SimpleDateFormat timeFormat;

    public EventOnTimeSetListener(Calendar cal, TextView textView, SimpleDateFormat sdf) {
        this.cal = cal;
        this.textView = textView;
        this.timeFormat = sdf;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);

        textView.setText(timeFormat.format(cal.getTime()));
    }
}
