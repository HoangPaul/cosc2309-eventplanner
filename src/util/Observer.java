package util;

/* Generics man. */
public interface Observer<T> {

    public void update(T o);
}
