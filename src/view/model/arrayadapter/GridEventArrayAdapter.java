package view.model;

import java.util.Calendar;
import java.util.List;

import util.Observer;
import util.Util;
import com.mad_ass2_3383484.R;

import model.EventInformation;
import model.ResourceLocator;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GridEventArrayAdapter extends ArrayAdapter<EventInformation> implements Observer<CalendarModel> {

    private enum CalendarState {
        DayTitle, BlankSpace, Day
    };

    private static final String[] DAYS = { "SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT" };
    private static final int NUMBER_OF_COLUMNS = 7;
    private static final int NUMBER_OF_ROWS = 7;

    private List<EventInformation> events;
    private Calendar month;

    private int positionClicked;

    private static class ViewHolder {
        private TextView toptext;
        private TextView midtext;
    }

    public GridEventArrayAdapter(Context context, int textViewResourceId, CalendarModel cm) {
        super(context, textViewResourceId, cm.getAllEvents());
        this.events = cm.getAllEvents();
        this.month = cm.getCalendar();
        this.month.set(Calendar.DATE, 1);
        positionClicked = -1;
        ResourceLocator.getCalendarModel().addObservers(this);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.getContext(), R.layout.grid_day_view, null);
            holder = new ViewHolder();
            holder.toptext = (TextView) convertView.findViewById(R.id.toptext);
            holder.midtext = (TextView) convertView.findViewById(R.id.midtext);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // Prevent bug where possible multiple grey squares
        convertView.setBackgroundColor(convertView.getResources().getColor(R.color.white));

        CalendarState currState = determineCalendarState(position);
        holder.toptext.setText("");
        holder.midtext.setText("");

        switch (currState) {
        case DayTitle:
            holder.toptext.setText(DAYS[position]);
            holder.toptext.setTextColor(Color.BLACK);
            break;
        case BlankSpace:
            holder.toptext.setText(" ");
            break;
        case Day:
            Calendar date = getDateFromPosition(position);
            int eventCount = calculateNumberOfEventsOnDay(date);
            holder.toptext.setText("" + date.get(Calendar.DATE));
            if (eventCount > 0) {
                String eventString = eventCount > 1 ? " events" : " event";
                holder.midtext.setText(eventCount + eventString);
            } else {
                holder.midtext.setText(" ");
            }
            if (this.positionClicked == position) {
                convertView.setBackgroundColor(convertView.getResources().getColor(R.color.light_grey));
            }
            break;
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return NUMBER_OF_COLUMNS * NUMBER_OF_ROWS;
    }

    /**
     * Converts a position on the gridview to an actual calendar value
     * 
     * @param calPos
     *            is the position on the gridview.
     * @return The date corresponding to the position on the gridview.
     */
    public Calendar getDateFromPosition(int position) {
        if (this.determineCalendarState(position) != CalendarState.Day) {
            return null;
        }
        Calendar date = Calendar.getInstance();
        date.set(Calendar.YEAR, month.get(Calendar.YEAR));
        date.set(Calendar.MONTH, month.get(Calendar.MONTH));
        date.set(Calendar.DAY_OF_MONTH, 1);

        int firstDayTitleInMonth = date.get(Calendar.DAY_OF_WEEK);
        int day = (position + 1) - NUMBER_OF_COLUMNS - (firstDayTitleInMonth - 1);

        date.set(Calendar.DAY_OF_MONTH, day);
        return date;
    }

    public void setPositionClicked(int position) {
        if (this.determineCalendarState(position) == CalendarState.Day) {
            positionClicked = position;
            this.notifyDataSetChanged();
        }
    }

    private CalendarState determineCalendarState(int position) {
        CalendarState state = CalendarState.BlankSpace;
        int firstDayTitleInMonth, lastDayInMonth;

        Calendar _firstDayInMonth = Calendar.getInstance();
        _firstDayInMonth.set(Calendar.YEAR, month.get(Calendar.YEAR));
        _firstDayInMonth.set(Calendar.MONTH, month.get(Calendar.MONTH));
        _firstDayInMonth.set(Calendar.DATE, 1);
        firstDayTitleInMonth = _firstDayInMonth.get(Calendar.DAY_OF_WEEK);

        lastDayInMonth = month.getActualMaximum(Calendar.DATE);

        if (position < NUMBER_OF_COLUMNS) {
            state = CalendarState.DayTitle;
        } else if (position - NUMBER_OF_COLUMNS < firstDayTitleInMonth - 1) {
            state = CalendarState.BlankSpace;
        } else if (position - NUMBER_OF_COLUMNS - (firstDayTitleInMonth - 1) < lastDayInMonth) {
            state = CalendarState.Day;
        } else {
            state = CalendarState.BlankSpace;
        }
        return state;
    }

    private int calculateNumberOfEventsOnDay(Calendar month) {
        int eventCount = 0;

        for (int i = 0; i < events.size(); i++) {
            if (Util.isSameDay(month, events.get(i).getCalendar())) {
                eventCount++;
            }
        }
        return eventCount;
    }

    @Override
    public void update(CalendarModel o) {
        this.month.set(Calendar.YEAR, o.getCalendar().get(Calendar.YEAR));
        this.month.set(Calendar.MONTH, o.getCalendar().get(Calendar.MONTH));
        this.events = o.getAllEvents();
        positionClicked = -1;
        this.notifyDataSetChanged();
    }
}
