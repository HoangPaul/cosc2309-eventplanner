package db;

import android.provider.BaseColumns;

public final class EventDatabaseContract {

    public static final int DATABASE_VERSION = 22;
    public static final String DATABASE_NAME = "eventdatabase.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA = ", ";

    private EventDatabaseContract() {
    }

    public static abstract class EventTable implements BaseColumns {
        public static final String TABLE_NAME = "event";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_VENUE = "venue";
        public static final String COLUMN_NAME_LONG = "long";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_NOTE = "note";
        public static final String COLUMN_NAME_ATTENDEES = "attendees";

        public static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY " + COMMA + COLUMN_NAME_TITLE + TEXT_TYPE + COMMA
                + COLUMN_NAME_TIME + TEXT_TYPE + COMMA + COLUMN_NAME_VENUE + TEXT_TYPE + COMMA + COLUMN_NAME_LONG + TEXT_TYPE + COMMA + COLUMN_NAME_LAT + TEXT_TYPE + COMMA
                + COLUMN_NAME_ATTENDEES + TEXT_TYPE + COMMA + COLUMN_NAME_NOTE + TEXT_TYPE + " )";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

}
