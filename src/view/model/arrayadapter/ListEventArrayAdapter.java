package view.model;

import java.util.List;

import model.EventInformation;
import model.IEventAdapter;
import android.content.Context;
import android.widget.ArrayAdapter;

public abstract class ListEventArrayAdapter extends ArrayAdapter<EventInformation> implements IEventAdapter {

    protected List<EventInformation> events;

    public ListEventArrayAdapter(Context context, int resource, List<EventInformation> objects) {
        super(context, resource, objects);
        events = objects;
    }

    @Override
    public EventInformation getEvent(int pos) {
        return events.get(pos);
    }

}
