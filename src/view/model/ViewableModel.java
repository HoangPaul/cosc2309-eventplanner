package view.model;

import java.util.ArrayList;
import java.util.List;

import model.EventInformation;
import model.IEventFinder;
import util.Observable;
import util.Observer;

/**
 * Subscribe to this or its subclasses to get up-to-date info on your model of
 * choice
 */
public abstract class ViewableModel<T extends ViewableModel<T>> implements Observable<T>, Observer<List<EventInformation>>, IEventFinder {

    protected List<EventInformation> events;
    protected ArrayList<Observer<T>> obs;

    public ViewableModel(Observable<List<EventInformation>> observable) {
        obs = new ArrayList<Observer<T>>();
        events = observable.addObservers(this);
    }

    public List<EventInformation> getEvents() {
        return this.events;
    }

    public void setEvents(List<EventInformation> newEvents) {
        this.events = newEvents;
    }

    @Override
    public EventInformation getEvent(int pos) {
        return events.get(pos);
    }

    @Override
    public int eventToPosition(EventInformation event) {
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).equals(event)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void updateObservers(T o) {
        for (int i = 0; i < obs.size(); i++) {
            obs.get(i).update(o);
        }
    }

}
