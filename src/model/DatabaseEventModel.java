package model;

import db.AddEventTask;
import db.DeleteEventTask;
import db.UpdateEventTask;
import android.content.Context;
import util.Util;

public class DatabaseEventModel extends BaseEventModel {

    public DatabaseEventModel() {
        super();
    }

    @Override
    public void addEvent(EventInformation e, Context context) {
        super.addEvent(e, context);
        Util.execute(new AddEventTask(context), e);
    }

    @Override
    public void removeEvent(EventInformation e, Context context) throws EventModelException {
        super.removeEvent(e, context);
        Util.execute(new DeleteEventTask(context), e);
    }

    @Override
    public void editEvent(EventInformation oldE, EventInformation newE, Context context) throws EventModelException {
        super.editEvent(oldE, newE, context);
        Util.execute(new UpdateEventTask(context), oldE, newE);
    }

}
