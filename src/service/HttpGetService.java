package service;

import util.Util;
import model.ResourceLocator;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;

public class HttpGetService extends Service {

    public static final int START_TIME = 0;
    public static final int REPEAT_TIME = 30 * 1000;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Log.i(Util.LOG_TAG, "updateThread started");
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location netLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (gpsLocation != null) {
            ResourceLocator.getEventModel().setLongitude(gpsLocation.getLongitude());
            ResourceLocator.getEventModel().setLatitude(gpsLocation.getLatitude());
        } else if (netLocation != null) {
            ResourceLocator.getEventModel().setLongitude(netLocation.getLongitude());
            ResourceLocator.getEventModel().setLatitude(netLocation.getLatitude());
        } else {
            return START_STICKY;
        }

        Context c = this.getApplicationContext();
        ResourceLocator.getEventModel().updateAllEventDistance(c);

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
