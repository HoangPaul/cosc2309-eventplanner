package http;

public class HttpQueryLimitException extends Exception {

    public HttpQueryLimitException() {
        super();
    }

    public HttpQueryLimitException(String msg) {
        super(msg);
    }

}
