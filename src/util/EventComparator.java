package model;

import java.util.Comparator;

public class EventComparator implements Comparator<EventInformation> {

    @Override
    public int compare(EventInformation lhs, EventInformation rhs) {
        return lhs.getCalendar().compareTo(rhs.getCalendar());
    }

}
