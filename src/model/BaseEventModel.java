package model;

import java.util.Collections;

import com.mad_ass2_3383484.R;

import android.content.Context;
import android.widget.Toast;

public class BaseEventModel extends AbstractEventModel {

    public BaseEventModel() {
        super();
    }

    @Override
    public void addEvent(EventInformation e, Context context) {
        synchronized (listEvents) {
            listEvents.add(e);
            Collections.sort(listEvents, new EventComparator());
        }
        super.updateAllEventDistance(context);
        this.updateObservers(this.listEvents);
        if (context != null)
            Toast.makeText(context, R.string.toastConfirmNewEvent, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void removeEvent(EventInformation e, Context context) throws EventModelException {
        if (!listEvents.contains(e)) {
            throw new EventModelException("Can't edit event - does not exist");
        }
        synchronized (listEvents) {
            listEvents.remove(e);
            Collections.sort(listEvents, new EventComparator());
        }
        super.updateAllEventDistance(context);
        this.updateObservers(this.listEvents);
        if (context != null)
            Toast.makeText(context, R.string.toastConfirmDeleteEvent, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void editEvent(EventInformation oldE, EventInformation newE, Context context) throws EventModelException {
        if (!listEvents.contains(oldE)) {
            throw new EventModelException("Can't edit event - does not exist");
        }
        synchronized (listEvents) {
            listEvents.remove(oldE);
            listEvents.add(newE);
            Collections.sort(listEvents, new EventComparator());
        }
        super.updateAllEventDistance(context);
        this.updateObservers(this.listEvents);
        if (context != null)
            Toast.makeText(context, R.string.toastConfirmEditEvent, Toast.LENGTH_SHORT).show();
    }

}
