package model;

import http.ThreadCallback;
import http.ValueHolder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.mad_ass2_3383484.R;

import db.UpdateModelTask;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import util.Observable;
import util.Observer;
import util.Util;
import view.MainActivity;

public abstract class AbstractEventModel implements Observable<List<EventInformation>> {

    public static final String EVENT_ID = "EventInModelPosition";

    public static final String TAG = "AbstractEventModel";
    public static final int NOTIFICATION_ID = 100;

    /* dummy values for testing */
    public static final double initLong = -37.8140000;
    public static final double initLat = 144.9633200;

    private boolean isFirstLoad;

    private double currLoclongitude;
    private double currLoclatitude;

    private Calendar timeSinceLastUpdate;

    List<EventInformation> listEvents;
    ArrayList<Observer<List<EventInformation>>> obs;

    public AbstractEventModel() {
        listEvents = Collections.synchronizedList(new ArrayList<EventInformation>());
        obs = new ArrayList<Observer<List<EventInformation>>>();
        this.currLoclongitude = initLong;
        this.currLoclatitude = initLat;
        this.isFirstLoad = true;
        this.timeSinceLastUpdate = Calendar.getInstance();
    }

    public abstract void addEvent(EventInformation e, Context context);

    public abstract void removeEvent(EventInformation e, Context context) throws EventModelException;

    public abstract void editEvent(EventInformation oldE, EventInformation newE, Context context) throws EventModelException;

    public void updateAllEventDistance(final Context context) {
        double tempLong = currLoclongitude;
        double tempLat = currLoclatitude;
        final AtomicInteger counter = new AtomicInteger(0);
        final int numberOfTasks = listEvents.size();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected) {
            Log.i(Util.LOG_TAG, "Updating!");
            for (int i = 0; i < this.listEvents.size(); i++) {
                if (listEvents.get(i).getCalendar().after(Calendar.getInstance())) {
                    /* Update distance information */
                    this.listEvents.get(i).updateAsyncDistanceInformation(this.currLoclongitude, this.currLoclatitude, new ThreadCallback<ValueHolder>() {
                        @Override
                        public void functionCallback(ValueHolder... params) {
                            incrementAndUpdate(counter, numberOfTasks, context);
                        }
                    });

                    /* "Chains" one event to the next */
                    if (this.listEvents.get(i).getLong() != 0 || this.listEvents.get(i).getLat() != 0) {
                        currLoclongitude = this.listEvents.get(i).getLong();
                        currLoclatitude = this.listEvents.get(i).getLat();
                    }
                } else {
                    incrementAndUpdate(counter, numberOfTasks, context);
                }
            }
        } else {
            Log.i(Util.LOG_TAG, "Tried to update, but no internet");
            sendNotification(context);
        }
        currLoclongitude = tempLong;
        currLoclatitude = tempLat;
    }

    /* Send notification and update observers after all ASyncTasks return */
    private void incrementAndUpdate(final AtomicInteger counter, final int numberOfTasks, final Context c) {
        counter.getAndIncrement();
        if (counter.get() == numberOfTasks) {
            this.timeSinceLastUpdate = Calendar.getInstance();
            if (c != null) {
                sendNotification(c);
            }
            this.updateModel(this.listEvents);

        }
    }

    public void sendNotification(Context c) {
        int count = 0;
        EventInformation firstEvent = null;
        Calendar futureCalendar = Calendar.getInstance();
        /* here be dragons */
        synchronized (listEvents) {
            Collections.sort(listEvents, new EventComparator());
            String prevLoc = "Current Location";
            boolean hasFoundValidEvent = false;
            for (int i = 0; i < listEvents.size(); i++) {
                EventInformation event = listEvents.get(i);
                if (Calendar.getInstance().after(event.getCalendar())) {
                    continue;
                }
                hasFoundValidEvent = true;
                /* Update distance strings */
                event.setPreviousLocation(prevLoc);
                if (event.getValueHolder().status == ValueHolder.Status.FOUND) {

                    prevLoc = event.getTitle();
                    if (event.getCalendar().after(Calendar.getInstance())) {
                        event.setIsAbleToMakeNextEvent(!event.isTimeToLeave(futureCalendar));
                    }
                }
                if (hasFoundValidEvent) {
                    futureCalendar = (Calendar) event.getCalendar().clone();
                }

                /* count for notification */
                if (event.isTimeToLeave(Calendar.getInstance())) {
                    count++;
                    if (firstEvent == null) {
                        firstEvent = event;
                    }
                }
            }
        }

        /* Derived from Google's developer guide */
        NotificationManager mNotificationManager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);

        if (firstEvent == null) {
            mNotificationManager.cancelAll();
            return;
        }

        String extraEvents = "";
        if (count > 1) {
            extraEvents = " and " + (count - 1) + " other(s)";
        }

        String contentText = firstEvent.getTitle() + extraEvents + " needs your help!";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(c).setSmallIcon(R.drawable.ic_action_place).setContentTitle("We need to leave!")
                .setContentText(contentText);

        Intent resultIntent = new Intent(c, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(c);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        // mId allows you to update the notification later on.
        mNotificationManager.notify(TAG, NOTIFICATION_ID, mBuilder.build());

    }

    public void updateModelFromDatabase(Context c) {
        if (isFirstLoad) {
            isFirstLoad = false;
            Util.execute(new UpdateModelTask(c, new ThreadCallback<ArrayList<EventInformation>>() {
                @SafeVarargs
                @Override
                public final void functionCallback(ArrayList<EventInformation>... params) {
                    updateModel(params[0]);
                }
            }));
        }
    }

    private void updateModel(List<EventInformation> newEvents) {
        synchronized (listEvents) {
            this.listEvents = Collections.synchronizedList(newEvents);
            Collections.sort(listEvents, new EventComparator());
        }
        this.updateObservers(this.listEvents);
    }

    public List<EventInformation> getAllEvents() {
        return this.listEvents;
    }

    public EventInformation getEvent(int pos) {
        return this.listEvents.get(pos);
    }

    public void setLongitude(double val) {
        this.currLoclongitude = val;
    }

    public void setLatitude(double val) {
        this.currLoclatitude = val;
    }

    public void updateObservers() {
        updateObservers(this.listEvents);
    }

    public Calendar getTimeSinceLastUpdate() {
        return (Calendar) this.timeSinceLastUpdate.clone();
    }

    @Override
    public void updateObservers(List<EventInformation> listEvents2) {
        for (int i = 0; i < obs.size(); i++) {
            obs.get(i).update(listEvents2);
        }
    }

    @Override
    public List<EventInformation> addObservers(Observer<List<EventInformation>> o) {
        obs.add(o);
        return this.listEvents;
    }
}
