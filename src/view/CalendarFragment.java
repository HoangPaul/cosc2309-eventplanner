package view;

import util.Observer;
import view.model.CalendarModel;
import view.model.GridEventArrayAdapter;
import view.model.ListAllEventArrayAdapter;
import model.ResourceLocator;
import model.controller.GridOnItemClickListener;
import model.controller.PreviousMonthOnClickListener;
import model.controller.RightMonthOnClickListener;

import com.mad_ass2_3383484.R;

import controller.ListAllOnItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class CalendarFragment extends Fragment implements Observer<CalendarModel> {

    CalendarModel cm;

    TextView monthTitle;

    public CalendarFragment() {
        super();
        this.cm = ResourceLocator.getCalendarModel();
        ResourceLocator.getCalendarModel().addObservers(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        // Show month name at the top of the calendar
        monthTitle = (TextView) rootView.findViewById(R.id.monthTitle);
        monthTitle.setText(cm.getMonthName());

        ImageButton leftButton = (ImageButton) rootView.findViewById(R.id.btnCalendarLeft);
        ImageButton rightButton = (ImageButton) rootView.findViewById(R.id.btnCalendarRight);
        leftButton.setOnClickListener(new PreviousMonthOnClickListener(cm));
        rightButton.setOnClickListener(new RightMonthOnClickListener(cm));

        // view for the calendar
        GridView gridview = (GridView) rootView.findViewById(R.id.gridview);

        GridEventArrayAdapter gridArrayAdapter = new GridEventArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, cm);
        gridview.setAdapter(gridArrayAdapter);
        gridview.setOnItemClickListener(new GridOnItemClickListener(gridArrayAdapter, cm));

        // view for the events on selected day
        ListView listview = (ListView) rootView.findViewById(R.id.listDayView);

        ListAllEventArrayAdapter<CalendarModel> listArrayAdapter = new ListAllEventArrayAdapter<CalendarModel>(getActivity(), R.layout.list_event_row, cm);
        listview.setAdapter(listArrayAdapter);
        listview.setOnItemClickListener(new ListAllOnItemClickListener(listArrayAdapter, cm));

        return rootView;
    }

    @Override
    public void update(CalendarModel o) {
        monthTitle.setText(cm.getMonthName());
    }
}
