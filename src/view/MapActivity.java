package view;

import http.ValueHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.EventInformation;
import model.ResourceLocator;
import view.model.EventWindowAdapter;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mad_ass2_3383484.R;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.model.Marker;

/* Code derived from Matt Witherow's example 
 * For no other reason than the sample code crashed my eclipse =(
 * 
 * Students were given sample code to use in their assignment.
 * Issues were encountered with sample code, so this is thrown 
 * together in a few hours just to meet Google's terms and conditions.
 * Buggy as hell, but requirements are met!
 **/
public class MapActivity extends FragmentActivity {

    private static final int DEFAULT_ZOOM = 8;
    private static final double RMIT_LAT = -37.8086141;
    private static final double RMIT_LNG = 144.963811;

    List<EventInformation> events;
    private Map<Marker, EventInformation> mModel;
    GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mModel = new HashMap<Marker, EventInformation>();

        this.events = ResourceLocator.getEventModel().getAllEvents();

        MapFragment mapFrag = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        map = mapFrag.getMap();

        EventWindowAdapter ewa = new EventWindowAdapter(this, mModel);

        map.setInfoWindowAdapter(ewa);

        for (int i = 0; i < events.size(); i++) {
            EventInformation event = events.get(i);
            if (event.getValueHolder().status == ValueHolder.Status.FOUND) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(new LatLng(event.getLat(), event.getLong()));
                Marker marker = map.addMarker(markerOptions);
                this.mModel.put(marker, event);
            }
        }
        LatLng latLng = new LatLng(RMIT_LAT, RMIT_LNG);
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.moveCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM));

    }

}