package db;

import http.ThreadCallback;

import java.util.ArrayList;

import util.Util;
import model.EventInformation;
import model.EventInformationBuilder;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class UpdateModelTask extends AsyncTask<EventInformation, Integer, Long> {

    Context context;
    Cursor cursor;
    ArrayList<EventInformation> events;
    final ThreadCallback<ArrayList<EventInformation>> callback;

    public UpdateModelTask(Context context, ThreadCallback<ArrayList<EventInformation>> callback) {
        this.context = context;
        events = new ArrayList<EventInformation>();
        this.callback = callback;
    }

    @Override
    protected Long doInBackground(EventInformation... event) {
        EventDatabaseHelper dbHelper = new EventDatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        long id = 0;

        /* SELECT * from event; */
        cursor = db.query(EventDatabaseContract.EventTable.TABLE_NAME, null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToPosition(-1);
            while (cursor.moveToNext()) {
                if (cursor == null) {
                    break;
                }
                id = cursor.getLong(cursor.getColumnIndex(EventDatabaseContract.EventTable._ID));
                events.add(new EventInformationBuilder().setID(id).setTitle(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_TITLE)))
                        .setTime(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_TIME)))
                        .setVenue(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_VENUE)))
                        .setLong(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_LONG)))
                        .setLat(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_LAT)))
                        .setNote(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_NOTE)))
                        .setAttendees(cursor.getString(cursor.getColumnIndex(EventDatabaseContract.EventTable.COLUMN_NAME_ATTENDEES))).buildEvent());
                Log.i(Util.LOG_TAG, id + " is the id");
            }

            cursor.close();
        } else {
            Log.i(Util.LOG_TAG, "cursor is null");
        }
        db.close();

        return id;
    }

    @Override
    protected void onPostExecute(Long result) {
        callback.functionCallback(events);
    }

}
