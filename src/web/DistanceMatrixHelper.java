package http;

import android.util.Log;
import util.Util;

public class DistanceMatrixHelper {

    private double initLong;
    private double initLang;
    private double destLong;
    private double destLang;

    private ValueHolder values;

    private ThreadCallback<ValueHolder> callback;

    public DistanceMatrixHelper() {
        values = null;
    }

    public DistanceMatrixHelper(ThreadCallback<ValueHolder> c) {
        this();
        this.callback = c;
    }

    public void setCallBack(ThreadCallback<ValueHolder> c) {
        this.callback = c;
    }

    public void executeHttpAsyncTaskRequest(final double initLong, final double initLang, final double destLong, final double destLang) {
        Log.i(Util.LOG_TAG, "this is " + initLong + ", " + initLang + "asd " + destLong + ", " + destLang);
        if (destLong == 0 && destLang == 0) {
            updateListener(ValueHolder.EMPTY_HOLDER);
        } else if (hasResultCached(initLong, initLang, destLong, destLang)) {
            updateListener(this.values);
        } else if (isSameLocation(initLong, initLang, destLong, destLang)) {
            cacheResults(ValueHolder.SAME_LOCATION_RESULT, initLong, initLang, destLong, destLang);
            updateListener(this.values);
        } else {
            Util.execute(new HttpAsync(new ThreadCallback<ValueHolder>() {
                @Override
                public void functionCallback(ValueHolder... results) {
                    cacheResults(results[0], initLong, initLang, destLong, destLang);
                    updateListener(results[0]);
                }
            }), initLong, initLang, destLong, destLang);
        }
    }

    private void cacheResults(ValueHolder result, double initLong, double initLang, double destLong, double destLang) {
        /* Cache the query for future use */
        this.initLong = initLong;
        this.initLang = initLang;
        this.destLong = destLong;
        this.destLang = destLang;
        this.values = result;
    }

    private boolean isSameLocation(double initLong, double initLang, double destLong, double destLang) {
        return initLong == destLong && initLang == destLang;
    }

    private boolean hasResultCached(double initLong, double initLang, double destLong, double destLang) {
        return this.values != null && this.initLong == initLong && this.initLang == initLang && this.destLong == destLong && this.destLang == destLang;
    }

    private void updateListener(ValueHolder value) {
        if (value != null && callback != null) {
            callback.functionCallback(value);
        }
    }

}
