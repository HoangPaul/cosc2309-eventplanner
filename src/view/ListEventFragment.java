package view;

import model.ResourceLocator;
import view.model.ListAllEventArrayAdapter;
import view.model.ListModel;

import com.mad_ass2_3383484.R;

import controller.ListAllOnItemClickListener;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ListEventFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_view, container, false);

        ListView listview = (ListView) rootView.findViewById(R.id.listview);
        ListAllEventArrayAdapter<ListModel> aa = new ListAllEventArrayAdapter<ListModel>(getActivity(), R.layout.list_event_row, ResourceLocator.getListModel());
        listview.setAdapter(aa);
        listview.setOnItemClickListener(new ListAllOnItemClickListener(aa, ResourceLocator.getListModel()));

        return rootView;
    }

}
