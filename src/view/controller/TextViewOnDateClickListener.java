package view.controller;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class TextViewOnDateClickListener implements OnClickListener {

    Calendar cal;
    DatePickerDialog.OnDateSetListener date;
    Context context;

    public TextViewOnDateClickListener(Calendar cal, DatePickerDialog.OnDateSetListener date, Context context) {
        this.cal = cal;
        this.date = date;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        new DatePickerDialog(context, date, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
    }

}
