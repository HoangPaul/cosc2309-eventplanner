package controller;

import model.EventInformation;
import model.BaseEventModel;
import model.IEventAdapter;
import model.IEventFinder;
import view.EditEventActivity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class ListAllOnItemClickListener implements OnItemClickListener {

    IEventAdapter listAllArrayAdapter;
    IEventFinder eFinder;

    public ListAllOnItemClickListener(IEventAdapter listAllArrayAdapter, IEventFinder eFinder) {
        this.listAllArrayAdapter = listAllArrayAdapter;
        this.eFinder = eFinder;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EventInformation event = listAllArrayAdapter.getEvent(position);
        if (event != null) {
            int indexInModel = eFinder.eventToPosition(event);
            Intent myIntent = new Intent(view.getContext(), EditEventActivity.class);
            myIntent.putExtra(BaseEventModel.EVENT_ID, indexInModel + "");
            view.getContext().startActivity(myIntent);
        }
    }

}
