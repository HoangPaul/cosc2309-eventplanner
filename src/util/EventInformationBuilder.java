package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import util.Util;

public class EventInformationBuilder {

    long id;
    String title;
    Calendar time;
    String venue;
    String note;
    double destLong;
    double destLat;
    List<String> attendees;

    public EventInformationBuilder() {
        this.id = 0;
        this.title = "";
        this.time = Calendar.getInstance();
        this.venue = "";
        this.note = "";
        this.destLong = 0;
        this.destLat = 0;
        this.attendees = new ArrayList<String>();
    }

    public EventInformationBuilder setID(long id) {
        this.id = id;
        return this;
    }

    public EventInformationBuilder setID(String id) {
        return setID(Long.parseLong(id));
    }

    public EventInformationBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public EventInformationBuilder setTime(Calendar time) {
        this.time = time;
        return this;
    }

    public EventInformationBuilder setTime(long time) {
        Calendar newCal = Calendar.getInstance();
        newCal.setTimeInMillis(time);
        return setTime(newCal);
    }

    public EventInformationBuilder setTime(String time) {
        return setTime(Long.parseLong(time));
    }

    public EventInformationBuilder setVenue(String venue) {
        this.venue = venue;
        return this;
    }

    public EventInformationBuilder setNote(String note) {
        this.note = note;
        return this;
    }

    public EventInformationBuilder setLong(double destLong) {
        this.destLong = destLong;
        return this;
    }

    public EventInformationBuilder setLat(double destLat) {
        this.destLat = destLat;
        return this;
    }

    public EventInformationBuilder setLong(String destLong) {
        if (destLong == null || destLong.isEmpty()) {
            return setLong(EventInformation.DEFAULT_LONG_VALUE);
        } else {
            return setLong(Double.parseDouble(destLong));
        }
    }

    public EventInformationBuilder setLat(String destLat) {
        if (destLat == null || destLat.isEmpty()) {
            return setLat(EventInformation.DEFAULT_LAT_VALUE);
        } else {
            return setLat(Double.parseDouble(destLat));
        }
    }

    public EventInformationBuilder setAttendees(List<String> att) {
        this.attendees = att;
        return this;
    }

    public EventInformationBuilder setAttendees(String att) {
        return setAttendees(Util.parseJsonToList(att));
    }

    public EventInformation buildEvent() {
        return new EventInformation(this.id, this.title, this.time, this.venue, this.note, this.destLong, this.destLat, this.attendees);
    }

}
