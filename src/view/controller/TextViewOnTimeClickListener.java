package view.controller;

import java.util.Calendar;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class TextViewOnTimeClickListener implements OnClickListener {

    Context context;
    TimePickerDialog.OnTimeSetListener time;
    Calendar cal;

    public TextViewOnTimeClickListener(Context context, TimePickerDialog.OnTimeSetListener time, Calendar cal) {
        this.context = context;
        this.time = time;
        this.cal = cal;
    }

    @Override
    public void onClick(View v) {
        new TimePickerDialog(context, time, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false).show();
    }
}
