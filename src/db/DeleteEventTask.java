package db;

import util.Util;
import model.EventInformation;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class DeleteEventTask extends AsyncTask<EventInformation, Integer, Long> {

    Context context;

    public DeleteEventTask(Context context) {
        this.context = context;
    }

    @Override
    protected Long doInBackground(EventInformation... event) {
        EventDatabaseHelper dbHelper = new EventDatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id = 0;

        String selection = EventDatabaseContract.EventTable._ID + " = ?";
        String[] selectionArgs = { String.valueOf(event[0].getID()) };

        db.beginTransaction();
        try {
            id = db.delete(EventDatabaseContract.EventTable.TABLE_NAME, selection, selectionArgs);
            if (id == 0) {
                Log.e(Util.LOG_TAG, "Error in deleting. id is " + event[0].getID());
            }
            db.setTransactionSuccessful();
        } catch (IllegalStateException e) {
            Log.i(Util.LOG_TAG, "DeleteEventTask transaction cannot be completed " + e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

        return id;
    }

}
