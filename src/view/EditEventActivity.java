package view;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import view.controller.EventOnDateSetListener;
import view.controller.EventOnTimeSetListener;
import view.controller.TextViewOnDateClickListener;
import view.controller.TextViewOnTimeClickListener;
import model.EventInformation;
import model.BaseEventModel;
import model.EventInformationBuilder;
import model.EventModelException;

import model.ResourceLocator;

import com.mad_ass2_3383484.R;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditEventActivity extends Activity {

    private Button btnConfirm;

    private Button btnDate;
    private Button btnTime;
    private EditText txtTitle;
    private EditText txtLong;
    private EditText txtLat;
    private EditText txtNote;
    private EditText txtVenue;

    private EventInformation editEvent;

    private Calendar cal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_event);

        setTitle(R.string.action_edit_event);

        String eventId = getIntent().getStringExtra(BaseEventModel.EVENT_ID);
        int pos = Integer.parseInt(eventId);
        editEvent = ResourceLocator.getEventModel().getEvent(pos);

        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnDate = (Button) findViewById(R.id.btnNewEventDate);
        btnTime = (Button) findViewById(R.id.btnNewEventTime);
        txtTitle = (EditText) findViewById(R.id.txtNewEventTitle);
        txtLong = (EditText) findViewById(R.id.txtNewEventLong);
        txtLat = (EditText) findViewById(R.id.txtNewEventLat);
        txtNote = (EditText) findViewById(R.id.txtNewEventNote);
        txtVenue = (EditText) findViewById(R.id.txtNewEventVenue);

        cal = editEvent.getCalendar();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy", Locale.UK);

        btnDate.setText(dateFormat.format(cal.getTime()));

        EventOnDateSetListener date = new EventOnDateSetListener(cal, btnDate, dateFormat);
        TextViewOnDateClickListener dateClickListener = new TextViewOnDateClickListener(cal, date, this);

        btnDate.setOnClickListener(dateClickListener);

        final SimpleDateFormat timeFormat = new SimpleDateFormat("h:m a", Locale.UK);
        btnTime.setText(timeFormat.format(cal.getTime()));

        TimePickerDialog.OnTimeSetListener time = new EventOnTimeSetListener(cal, btnTime, timeFormat);
        TextViewOnTimeClickListener timeClickListener = new TextViewOnTimeClickListener(this, time, cal);
        btnTime.setOnClickListener(timeClickListener);

        txtTitle.setText(editEvent.getTitle());
        txtLong.setText(String.valueOf(editEvent.getLong()));
        txtLat.setText(String.valueOf(editEvent.getLat()));
        txtNote.setText(editEvent.getNote());
        txtVenue.setText(editEvent.getVenue());
        btnConfirm.setText(R.string.btnNewEventConfirm);

        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmEdit();
                finish();
            }
        });
    }

    private void confirmEdit() {
        String title;
        String longitude, lat;
        String note;
        String venue;
        Calendar time;

        title = txtTitle.getText().toString();
        longitude = txtLong.getText().toString();
        lat = txtLat.getText().toString();
        note = txtNote.getText().toString();
        venue = txtVenue.getText().toString();
        time = cal;

        EventInformation e = new EventInformationBuilder().setID(editEvent.getID()).setTitle(title).setNote(note).setVenue(venue).setTime(time).setLong(longitude).setLat(lat)
                .buildEvent();
        try {
            ResourceLocator.getEventModel().editEvent(editEvent, e, this.getBaseContext());
        } catch (EventModelException ex) {
            ex.printStackTrace();
        }

        setResult(Activity.RESULT_OK);
    }

}
