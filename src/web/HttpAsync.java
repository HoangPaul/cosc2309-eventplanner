package http;

import android.os.AsyncTask;

public class HttpAsync extends AsyncTask<Double, Void, ValueHolder> {

    ThreadCallback<ValueHolder> callback;

    public HttpAsync(ThreadCallback<ValueHolder> callback) {
        this.callback = callback;
    }

    protected ValueHolder doInBackground(Double... coords) {
        ValueHolder value = DistanceMatrixCode.requestDurationData(coords[0], coords[1], coords[2], coords[3]);
        return value;
    }

    @Override
    protected void onPostExecute(ValueHolder result) {
        this.callback.functionCallback(result);
    }

}
