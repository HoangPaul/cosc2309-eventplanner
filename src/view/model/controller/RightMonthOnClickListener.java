package model.controller;

import view.model.CalendarModel;
import android.view.View;
import android.view.View.OnClickListener;

public class RightMonthOnClickListener implements OnClickListener {

    CalendarModel cm;

    public RightMonthOnClickListener(CalendarModel cm) {
        this.cm = cm;
    }

    @Override
    public void onClick(View v) {
        cm.setToNextMonth();
    }

}
