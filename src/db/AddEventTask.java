package db;

import util.Util;
import model.EventInformation;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class AddEventTask extends AsyncTask<EventInformation, Integer, Long> {

    Context context;

    public AddEventTask(Context context) {
        this.context = context;
    }

    @Override
    protected Long doInBackground(EventInformation... event) {
        EventDatabaseHelper dbHelper = new EventDatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        long id = 0;

        db.beginTransaction();
        try {
            for (int i = 0; i < event.length; i++) {
                values = event[i].populateContentValues(values);
                id = db.insert(EventDatabaseContract.EventTable.TABLE_NAME, null, values);
                if (id == -1) {
                    Log.e(Util.LOG_TAG, "Error in inserting");
                }
                event[i].setID(id);
                Log.i(Util.LOG_TAG, "Inserted id " + id);
            }
            db.setTransactionSuccessful();
        } catch (IllegalStateException e) {
            Log.i(Util.LOG_TAG, "AddEventTask transaction cannot be completed " + e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

        return id;
    }

}
